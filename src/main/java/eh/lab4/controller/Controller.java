package eh.lab4.controller;

import eh.lab4.entities.Patient;
import eh.lab4.service.Service;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/controller")
public class Controller {
    @Autowired
    private Service service;
    @GetMapping()
    public String index() throws JSONException {
        List<Patient> patients = service.getPatients();
        String info = "";

        for (Patient p: patients) {
            info += "Resource type: " + p.getResourceType() + "<br/>" +
                    "Id: " + p.getId() + "<br/>" +
                    "Name: " + p.getName() + "<br/>" +
                    "Active: " + p.getActive() + "<br/>" +
                    "Gender: " + p.getGender() + "<br/>" +
                    "Phone: " + p.getTelecom() + "<br/>" +
                    "Birthdate: " + p.getBirthDate() + "<br/>" +
                    "Deceased: " + p.getDeceasedBoolean() + "<br/>" +
                    "Address: " + p.getAddress() + "<br/>" +
                    "Contact person phone: " + p.getContact() + "<br/><br/>";

        }

        return info;
    }
}
