package eh.lab4.entities;

public class Patient {
    private String resourceType;
    private String id;
    private String identifier;
    private String active;
    private String name;
    private String telecom;
    private String gender;
    private String birthDate;
    private String deceasedBoolean;
    private String address;
    private String contact;

    public Patient(String resourceType, String id, String identifier, String active, String name, String telecom, String gender, String birthDate, String deceasedBoolean, String address, String contact) {
        this.resourceType = resourceType;
        this.id = id;
        this.identifier = identifier;
        this.active = active;
        this.name = name;
        this.telecom = telecom;
        this.gender = gender;
        this.birthDate = birthDate;
        this.deceasedBoolean = deceasedBoolean;
        this.address = address;
        this.contact = contact;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelecom() {
        return telecom;
    }

    public void setTelecom(String telecom) {
        this.telecom = telecom;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDeceasedBoolean() {
        return deceasedBoolean;
    }

    public void setDeceasedBoolean(String deceasedBoolean) {
        this.deceasedBoolean = deceasedBoolean;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
